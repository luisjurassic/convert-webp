﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Convert.Images
{
    public static class Helpers
    {
        public static string TextoAntesDoUltimoSeletor(this string value, string selector)
        {
            int index = value.LastIndexOf(selector);
            return index == -1 ? value : value.Substring(0, index);
        }

        public static ScrollViewer FindScrollViewer(DependencyObject root)
        {
            Queue<DependencyObject> queue = new Queue<DependencyObject>(new[] { root });

            do
            {
                DependencyObject item = queue.Dequeue();

                if (item is ScrollViewer viewer)
                {
                    return viewer;
                }

                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(item); i++)
                {
                    queue.Enqueue(VisualTreeHelper.GetChild(item, i));
                }
            } while (queue.Count > 0);

            return null;
        }
    }
}
