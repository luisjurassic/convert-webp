﻿using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;

namespace Convert.Images
{
    public partial class MainWindow : Window
    {
        private CancellationToken cancellationToken;
        private CancellationTokenSource cancellationTokenSource;
        public ImagemModel Model { get; private set; }

        public MainWindow()
        {
            InitializeComponent();
            Model = new ImagemModel();
            DataContext = Model;
        }

        private void ConverterClick(object sender, RoutedEventArgs e)
        {
            cancellationTokenSource = new CancellationTokenSource();
            cancellationToken = cancellationTokenSource.Token;
            System.Windows.Application.Current.MainWindow.Cursor = System.Windows.Input.Cursors.Wait;

            _ = Task.Factory.StartNew(() =>
            {
                Model.Converter();
                ObservableCollection<string> logs = Model.Logs;
                Model = new ImagemModel
                {
                    Logs = logs
                };
                _ = Dispatcher.Invoke(() => DataContext = Model);

                _ = System.Windows.MessageBox.Show("Conversão das imagens concluida.", "Conversão Concluida", MessageBoxButton.OK, MessageBoxImage.Information);
            }, cancellationToken);
        }

        private void LimparLogClick(object sender, RoutedEventArgs e)
        {
            Model.Logs = new ObservableCollection<string>();
            DataContext = Model;
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            if (!cancellationTokenSource?.IsCancellationRequested ?? false)
            {
                cancellationTokenSource.Cancel();
                _ = System.Windows.MessageBox.Show("Processo de conversão interrompido.", "Processo Interrompido", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void OrigemLocaisClick(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog()
            {
                Description = "Selecione o local",
                ShowNewFolderButton = false,
            };
            if (folderBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Model.LocalOrigem = folderBrowserDialog.SelectedPath;
                Model.ArquivosOrigem = Directory.EnumerateFiles(Model.LocalOrigem, "*.*", SearchOption.AllDirectories)
                    .Where(s => s.EndsWith(".jpg") || s.EndsWith(".jpeg") || s.EndsWith(".png") || s.EndsWith(".gif") || s.EndsWith(".bmp") || s.EndsWith(".webp")).ToList();

                Model.Descricao = string.Join(",", Model.ArquivosOrigem);

                Model.Logs.Add(Model.Descricao);
                Model.Logs.Add($"{Model.ArquivosOrigem.Count} arquivo(s)");
            }
        }

        private void OrigemArquivosClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Multiselect = true,
                Filter = "Image Files|*.jpg;*.jpeg;*.png;*.gif;*.bmp;*.webp;",
                CheckFileExists = true,
                Title = "Selecione os arquivos",
            };
            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Model.ArquivosOrigem = openFileDialog.FileNames.ToList();
                Model.Descricao = $"{Model.ArquivosOrigem.Count} arquivo(s)";

                Model.Logs.Add(Model.Descricao);
            }
        }

        private void ListBox_Loaded(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.ListBox listBox = (System.Windows.Controls.ListBox)sender;

            ScrollViewer scrollViewer = Helpers.FindScrollViewer(listBox);

            if (scrollViewer != null)
            {
                scrollViewer.ScrollChanged += (o, args) =>
                {
                    if (args.ExtentHeightChange > 0)
                    {
                        scrollViewer.ScrollToBottom();
                    }
                };
            }
        }
    }
}
