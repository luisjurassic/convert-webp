﻿using ImageProcessor;
using ImageProcessor.Plugins.WebP.Imaging.Formats;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using System.Collections.Generic;
using System;
using ImageProcessor.Imaging.Formats;

namespace Convert.Images
{
    public class ImagemModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private List<string> _ArquivosOrigem;
        private string _LocalOrigem;
        private string _Descricao;
        private byte _Qualidade;
        private uint _Altura;
        private uint _Largura;
        private bool _ManterProporcao;
        private bool _ProporcaoCustomizada;
        private bool _OrigemLocais;
        private bool _OrigemArquivos;
        private int _Tipo;
        private ObservableCollection<string> _Logs;

        public List<string> ArquivosOrigem
        {
            get => _ArquivosOrigem;
            set
            {
                _ArquivosOrigem = value;
                OnChanged();
            }
        }
        public string LocalOrigem
        {
            get => _LocalOrigem;
            set
            {
                _LocalOrigem = value;
                OnChanged();
            }
        }
        public string Descricao
        {
            get => _Descricao;
            set
            {
                _Descricao = value;
                OnChanged();
            }
        }
        public byte Qualidade
        {
            get => _Qualidade;
            set
            {
                _Qualidade = value;
                OnChanged();
            }
        }
        public uint Altura
        {
            get => _Altura;
            set
            {
                _Altura = value;
                OnChanged();
            }
        }
        public uint Largura
        {
            get => _Largura;
            set
            {
                _Largura = value;
                OnChanged();
            }
        }
        public bool ManterProporcao
        {
            get => _ManterProporcao;
            set
            {
                _ManterProporcao = value;
                OnChanged();
            }
        }
        public bool ProporcaoCustomizada
        {
            get => _ProporcaoCustomizada;
            set
            {
                _ProporcaoCustomizada = value;
                OnChanged();
            }
        }
        public bool OrigemLocais
        {
            get => _OrigemLocais;
            set
            {
                _OrigemLocais = value;
                OnChanged();
            }
        }
        public bool OrigemArquivos
        {
            get => _OrigemArquivos;
            set
            {
                _OrigemArquivos = value;
                OnChanged();
            }
        }
        public int Tipo
        {
            get => _Tipo;
            set
            {
                _Tipo = value;
                OnChanged();
            }
        }
        public ObservableCollection<string> Logs
        {
            get => _Logs;
            set
            {
                _Logs = value;
                OnChanged();
            }
        }

        public void OnChanged([CallerMemberName] string sender = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(sender));
        }

        public ImagemModel()
        {
            ArquivosOrigem = new List<string>();
            Qualidade = 80;
            ManterProporcao = true;
            OrigemArquivos = true;
            Logs = new ObservableCollection<string>();
        }

        public void Converter()
        {
            foreach (string item in ArquivosOrigem)
            {
                try
                {
                    FileInfo file = new FileInfo(item);
                    string path = Path.Combine(file.Directory.FullName);
                    if (!Directory.Exists(path))
                    {
                        _ = Directory.CreateDirectory(path);
                    }
                     
                    if ((ImageType)Tipo == ImageType.WebP)
                    {
                        WebPFormatConvert(file, path);
                    }
                    else if ((ImageType)Tipo == ImageType.PNG)
                    {
                        PngFormatConvert(file, path);
                    }
                    else if ((ImageType)Tipo == ImageType.JPEG)
                    {
                        JpegFormatConvert(file, path);
                    }
                    else if ((ImageType)Tipo == ImageType.GIF)
                    {
                        GifFormatConvert(file, path);
                    }
                    else if ((ImageType)Tipo == ImageType.BMP)
                    {
                        BitmapFormatConvert(file, path);
                    }
                }
                catch (Exception ex)
                {
                    Application.Current.Dispatcher.Invoke(() => Logs.Add($"{ex.Message} Arquivo '{item}'"));
                }
            }

            _ = Application.Current.Dispatcher.Invoke(() => Application.Current.MainWindow.Cursor = Cursors.Arrow);
        }

        private void WebPFormatConvert(FileInfo file, string path)
        {
            string newFile = Path.Combine(path, file.Name.TextoAntesDoUltimoSeletor(".") + ".webp");
            using (FileStream fileStream = file.OpenRead())
            {
                using (FileStream newFileStream = new FileStream(newFile, FileMode.Create))
                {
                    using (ImageFactory imageFactory = new ImageFactory())
                    {
                        if (ManterProporcao)
                        {
                            _ = imageFactory.Load(fileStream)
                                        .Format(new WebPFormat())
                                        .Quality(Qualidade)
                                        .Save(newFileStream);
                        }
                        else if (ProporcaoCustomizada)
                        {
                            _ = imageFactory.Load(fileStream)
                                        .Format(new WebPFormat())
                                        .Resize(new System.Drawing.Size((int)Largura, (int)Altura))
                                        .Quality(Qualidade)
                                        .Save(newFileStream);
                        }
                    }
                }
            }

            Application.Current.Dispatcher.Invoke(() => Logs.Add($"{file.FullName} para {newFile}"));
        }

        private void PngFormatConvert(FileInfo file, string path)
        {
            string newFile = Path.Combine(path, file.Name.TextoAntesDoUltimoSeletor(".") + ".png");
            using (FileStream fileStream = file.OpenRead())
            {
                using (FileStream newFileStream = new FileStream(newFile, FileMode.Create))
                {
                    using (ImageFactory imageFactory = new ImageFactory())
                    {
                        if (ManterProporcao)
                        {
                            _ = imageFactory.Load(fileStream)
                                        .Format(new PngFormat())
                                        .Quality(Qualidade)
                                        .Save(newFileStream);
                        }
                        else if (ProporcaoCustomizada)
                        {
                            _ = imageFactory.Load(fileStream)
                                        .Format(new PngFormat())
                                        .Resize(new System.Drawing.Size((int)Largura, (int)Altura))
                                        .Quality(Qualidade)
                                        .Save(newFileStream);
                        }
                    }
                }
            }

            Application.Current.Dispatcher.Invoke(() => Logs.Add($"{file.FullName} para {newFile}"));
        }

        private void JpegFormatConvert(FileInfo file, string path)
        {
            string newFile = Path.Combine(path, file.Name.TextoAntesDoUltimoSeletor(".") + ".jpeg");
            using (FileStream fileStream = file.OpenRead())
            {
                using (FileStream newFileStream = new FileStream(newFile, FileMode.Create))
                {
                    using (ImageFactory imageFactory = new ImageFactory())
                    {
                        if (ManterProporcao)
                        {
                            _ = imageFactory.Load(fileStream)
                                        .Format(new JpegFormat())
                                        .Quality(Qualidade)
                                        .Save(newFileStream);
                        }
                        else if (ProporcaoCustomizada)
                        {
                            _ = imageFactory.Load(fileStream)
                                        .Format(new JpegFormat())
                                        .Resize(new System.Drawing.Size((int)Largura, (int)Altura))
                                        .Quality(Qualidade)
                                        .Save(newFileStream);
                        }
                    }
                }
            }

            Application.Current.Dispatcher.Invoke(() => Logs.Add($"{file.FullName} para {newFile}"));
        }

        private void GifFormatConvert(FileInfo file, string path)
        {
            string newFile = Path.Combine(path, file.Name.TextoAntesDoUltimoSeletor(".") + ".gif");
            using (FileStream fileStream = file.OpenRead())
            {
                using (FileStream newFileStream = new FileStream(newFile, FileMode.Create))
                {
                    using (ImageFactory imageFactory = new ImageFactory())
                    {
                        if (ManterProporcao)
                        {
                            _ = imageFactory.Load(fileStream)
                                        .Format(new GifFormat())
                                        .Quality(Qualidade)
                                        .Save(newFileStream);
                        }
                        else if (ProporcaoCustomizada)
                        {
                            _ = imageFactory.Load(fileStream)
                                        .Format(new GifFormat())
                                        .Resize(new System.Drawing.Size((int)Largura, (int)Altura))
                                        .Quality(Qualidade)
                                        .Save(newFileStream);
                        }
                    }
                }
            }

            Application.Current.Dispatcher.Invoke(() => Logs.Add($"{file.FullName} para {newFile}"));
        }

        private void BitmapFormatConvert(FileInfo file, string path)
        {
            string newFile = Path.Combine(path, file.Name.TextoAntesDoUltimoSeletor(".") + ".bmp");
            using (FileStream fileStream = file.OpenRead())
            {
                using (FileStream newFileStream = new FileStream(newFile, FileMode.Create))
                {
                    using (ImageFactory imageFactory = new ImageFactory())
                    {
                        if (ManterProporcao)
                        {
                            _ = imageFactory.Load(fileStream)
                                        .Format(new BitmapFormat())
                                        .Quality(Qualidade)
                                        .Save(newFileStream);
                        }
                        else if (ProporcaoCustomizada)
                        {
                            _ = imageFactory.Load(fileStream)
                                        .Format(new BitmapFormat())
                                        .Resize(new System.Drawing.Size((int)Largura, (int)Altura))
                                        .Quality(Qualidade)
                                        .Save(newFileStream);
                        }
                    }
                }
            }

            Application.Current.Dispatcher.Invoke(() => Logs.Add($"{file.FullName} para {newFile}"));
        }

    }
}
