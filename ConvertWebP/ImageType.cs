﻿namespace Convert.Images
{
    public enum ImageType
    {
        WebP = 0,
        PNG = 1,
        JPEG = 2,
        BMP = 3,
        GIF = 4,
    }
}
